<?php

// Mini projet hotel avec tableau multidimensionnel

include "../tableau_multi_avec_fonctions/function.php";
include "../tableau_multi_avec_fonctions/utils.php";

$passwrd = "toto";
$login = "titi";
$nombreChambres = 20;

$chambres = initialisationDesChambres($nombreChambres);

while (true) {

    include "menu.php";

    if ($choixMenu === "Q") {

        break;
    } elseif ($choixMenu === "A") {

        etatHotel($chambres);

    } elseif ($choixMenu === "B") {

        nbreChambresOcupees($chambres);

    } elseif ($choixMenu === "C") {

        nbreChambresLibres($chambres);

    } elseif ($choixMenu === "D") {

        premChambreVide($chambres);

    } elseif ($choixMenu === "E") {

        dernChambreVide($chambres);

    } elseif ($choixMenu === "F") {

        securiteLogin($login, $passwrd);
        reserverPremVide($chambres);
        var_dump($chambres);

    } elseif ($choixMenu === "G") {

        securiteLogin($login, $passwrd);
        libererDernOccupee($chambres);
    }
}
