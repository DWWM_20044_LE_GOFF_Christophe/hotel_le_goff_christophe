<?php

// Répartition des chambres

function initialisationDesChambres($combienDeChambres)
{
    for ($i = 1; $i <= $combienDeChambres; $i++) {
        $chambre["numero"] = $i;
        $estLibre = random_int(0, 1);
        $chambre["etat"] = $estLibre;
        $prix = [15, 25, 35, 45];
        $chambre["prix"] = $prix[array_rand($prix, 1)];
        $chambres[] = $chambre;
    }
    return $chambres;
}

// fonction pour la sécurité (Menu F et G)

function securiteLogin($utilisateur, $motDePasse)
{
    $loginSaisie = readline("Entrer votre nom d'utilisateur : ");
    while ($utilisateur != $loginSaisie) {
        change_color("red");
        $loginSaisie = readline("Nom d'utilisateur incorrect (resaisire svp) : ");
        change_color("");
    }
    $passwrdSaisie = readline("Entrer votre mot de passe : ");
    while ($motDePasse != $passwrdSaisie) {
        change_color("red");
        $passwrdSaisie = readline("Mot de passe incorrect (resaisire svp) : ");
        change_color("");
    }
}

// Menu A : Affichage de l'état de l'hotel

function etatHotel($listeChambres)
{
    echo ("Occupation de l'hotel : " . PHP_EOL . PHP_EOL);
    $cptLibre = 0;
    $cptOccupee = 0;
    foreach ($listeChambres as $key => $uneChambre) {
        foreach ($uneChambre as $keys => $etat) {

            if ($keys === "etat" && $etat === 1) {
                change_color("purple");
                echo ("Chambre : " . $listeChambres[$key]["numero"] . " : occupée." . PHP_EOL);
                $cptOccupee++;
            }
            if ($keys === "etat" && $etat === 0) {
                change_color("green");
                echo ("Chambre : " . $listeChambres[$key]["numero"] . " : libre." . PHP_EOL);
                $cptLibre++;
            }
        }
    }
    change_color("blue");
    echo (PHP_EOL);
    echo ($cptOccupee . " chambre(s) occupées et " .  $cptLibre . " chambre(s) libres." . PHP_EOL);
    change_color("");
    $cptLibre = 0;
    $cptOccupee = 0;
}

// Menu B : Affichage du nombre de chambres occupees

function nbreChambresOcupees($listeChambres)
{

    $cptOccupee = 0;
    foreach ($listeChambres as $key => $uneChambre) {
        foreach ($uneChambre as $keys => $etat) {

            if ($keys === "etat" && $etat === 1) {
                $cptOccupee++;
            }
        }
    }
    change_color("blue");
    echo ($cptOccupee . " chambre(s) réservées." . PHP_EOL);
    change_color("");
}

// Menu C : Affichage du nombre de chambres libres

function nbreChambresLibres($listeChambres)
{

    $cptLibre = 0;
    foreach ($listeChambres as $key => $uneChambre) {
        foreach ($uneChambre as $keys => $etat) {

            if ($keys === "etat" && $etat === 0) {
                $cptLibre++;
            }
        }
    }
    change_color("blue");
    echo ($cptLibre . " chambre(s) libre(s)." . PHP_EOL);
    change_color("");
}

// Menu D : affichage de la première chambre vide

function premChambreVide($listeChambres)
{

    $cptLibre = 0;
    foreach ($listeChambres as $key => $uneChambre) {
        foreach ($uneChambre as $keys => $etat) {

            if ($keys === "etat" && $etat === 0) {
                change_color("blue");
                echo ("La première chambre vide est la numéro " . $listeChambres[$key]["numero"] . PHP_EOL);
                change_color("");
                $cptLibre = 1;
                break;
            }
        }
        if ($cptLibre === 1) {
            break;
        }
    }
    if ($cptLibre === 0) 
    {
        change_color("red");
        echo ("Il n'y a pas de chambre vide!" . PHP_EOL);
        change_color("");
    }
}

// Menu E : Affichage de la dernière chambre vide

function dernChambreVide($listeChambres)
{
    $cptLibre = 0;
    for ($i = count($listeChambres) - 1; $i >= 0; $i--) {

        if ($listeChambres[$i]["etat"] === 0) {
            change_color("blue");
            echo ("La dernère chambre vide est la numéro : " . $listeChambres[$i]["numero"] . PHP_EOL);
            change_color("");
            $cptLibre++;
            break;
        }
    }
    if ($cptLibre === 0) {
        change_color("red");
        echo ("Toutes les chambres sont occupée(s)" . PHP_EOL);
        change_color("");
    }
}

// Menu F : F-	Réserver une chambre (Le programme doit réserver la première chambre vide)

function reserverPremVide(&$listeChambres)
{

    $estreservee = 0;
    foreach ($listeChambres as $numeroChambre => $uneChambre) {

        foreach ($uneChambre as $key => $libreOuOccupee) {

            if ($key === "etat" && $libreOuOccupee === 0) {
                $listeChambres[$numeroChambre]["etat"] = 1;
                change_color("blue");
                echo (" La chambre numéro : " .  $listeChambres[$numeroChambre]["numero"] . " est maintenant réservée." . PHP_EOL);
                change_color("");
                $estreservee = 1;
                break;
            }
            if ($estreservee === 1) {
                break;
            }
            $estreservee = 0;
        }
    }

    if ($estreservee === 0 && $numeroChambre === (count($listeChambres) - 1)) {
        change_color("red");
        echo ("Toutes les chambres sont occupées!" . PHP_EOL);
        change_color("");
    }
}

// menu G :	Libérer une chambre (Le programme doit libérer la dernière chambre occupée)

function libererDernOccupee(&$chambres)
{
    $cpt = 0;
    for ($i = count($chambres) - 1; $i >= 0; $i--) {

        if ($chambres[$i]["etat"] === 1) {
            $chambres[$i]["etat"] = 0;
            change_color("blue");
            echo ("La chambre numéro : " . $chambres[$i]["numero"] . " est maintenant libérée." . PHP_EOL);
            change_color("");
            $cpt++;
            break;
        } elseif ($cpt === 0 && $i === 0) {
            change_color("red");
            echo ("Toutes les chambres sont déjà libres!" . PHP_EOL);
            change_color("");
        }
    }
}
