<?php

echo (PHP_EOL . "-------------------------------  MENU HOTEL DWWM PHP  ----------------------" . PHP_EOL .
    "   A- Afficher l'état de l'hôtel" . PHP_EOL .
    "   B- Afficher le nombre de chambres réservées" . PHP_EOL .
    "   C- Afficher le nombre de chambre libres" . PHP_EOL .
    "   D- Afficher le numéro de la première chambre vide" . PHP_EOL .
    "   E- Afficher le numéro de la dernière chambre vide" . PHP_EOL .
    "   F- Réserver une chambre" . PHP_EOL .
    "   G- Libérer une chambre" . PHP_EOL . PHP_EOL .
    "   Q- Quitter" . PHP_EOL .
    "----------------------------------------------------------------------------" . PHP_EOL . PHP_EOL);
$choixMenu = strtoupper(readline("Votre choix  : "));
while (true) {
    if ($choixMenu != "A" && $choixMenu != "B" && $choixMenu != "C" && $choixMenu != "D" && $choixMenu != "E" && $choixMenu != "F" && $choixMenu != "G" && $choixMenu != "Q") {
        change_color("red");
        $choixMenu = strtoupper(readline("choix  invalide : "));
        change_color("");
    }
break;
}
