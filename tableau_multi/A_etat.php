<?php

echo ("Occupation de l'hotel : " . PHP_EOL . PHP_EOL);

foreach ($chambres as $key => $uneChambre) {
    foreach ($uneChambre as $keys => $etat) {


        if ($keys === "etat" && $etat === 1) {
            echo("Chambre : " . $chambres[$key]["numero"] . " : occupée." . PHP_EOL);
            $cptOccupee++;
        }
        if ($keys === "etat" && $etat === 0) {
            echo("Chambre : " . $chambres[$key]["numero"] . " : libre." . PHP_EOL);
            $cptLibre++;
        }
    }
}

echo(PHP_EOL);
echo ($cptOccupee . " chambre(s) occupées et " .  $cptLibre . " chambre(s) libres." . PHP_EOL);

$cptLibre = 0;
$cptOccupee = 0;

?>