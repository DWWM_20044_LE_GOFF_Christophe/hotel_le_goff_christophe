<?php

$loginSaisie = readline("Entrer votre nom d'utilisateur : ");
echo (PHP_EOL);
while ($login != $loginSaisie) {
    $loginSaisie = readline("Nom d'utilisateur incorrect (resaisire svp) : ");
}
echo (PHP_EOL);
$passwrdSaisie = readline("Entrer votre mot de passe : ");
echo (PHP_EOL);
while ($passwrd != $passwrdSaisie) {
    $passwrdSaisie = readline("Mot de passe incorrect (resaisire svp) : ");
}

$cpt = 0;
for ($i = count($chambres) - 1; $i >= 0; $i--) {

    if ($chambres[$i]["etat"] === 1) {
        $chambres[$i]["etat"] = 0;
        echo ("La chambre numéro : " . $chambres[$i]["numero"] . " est maintenant libérée." . PHP_EOL);
        $cpt++;
        break;
    } elseif ($cpt === 0 && $i === 0) {
        echo ("Toutes les chambres sont déjà libres!" . PHP_EOL);
    }
}
