<?php

$loginSaisie = readline("Entrer votre nom d'utilisateur : ");
echo (PHP_EOL);
while ($login != $loginSaisie) {
    $loginSaisie = readline("Nom d'utilisateur incorrect (resaisire svp) : ");
}
echo (PHP_EOL);
$passwrdSaisie = readline("Entrer votre mot de passe : ");
echo (PHP_EOL);
while ($passwrd != $passwrdSaisie) {
    $passwrdSaisie = readline("Mot de passe incorrect (resaisire svp) : ");
}

$estreservee = 0;
foreach ($chambres as $numeroChambre => $uneChambre) {

    foreach ($uneChambre as $key => $libreOuOccupee) {

        if ($key === "etat" && $libreOuOccupee === 0) {
            $chambres[$numeroChambre]["etat"] = 1;
            echo (" La chambre numéro : " .  $chambres[$numeroChambre]["numero"] . " est maintenant réservée." . PHP_EOL);
            $estreservee = 1;
            break;
        }
        if ($estreservee === 1) {
            break;
        }
        $estreservee = 0;
    }
}

if ($estreservee === 0 && $numeroChambre === (count($chambres) - 1)) {
    echo ("Toutes les chambres sont occupées!" . PHP_EOL);
}
