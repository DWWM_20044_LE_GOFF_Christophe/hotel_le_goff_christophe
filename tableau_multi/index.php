<?php

// Mini projet hotel avec tableau multidimensionnel

$cptLibre = 0;
$cptOccupee = 0;

$passwrd = "toto";
$login = "titi";

for ($i = 1; $i <= 20; $i++) {
    $chambre["numero"] = $i;
    $estLibre = random_int(0, 1);
    $chambre["etat"] = $estLibre;
    $prix = [15, 25, 35, 45];
    $chambre["prix"] = $prix[array_rand($prix, 1)];

    $chambres[] = $chambre;
}

while (true) {

    include "menu.php";

    while (true) {
        if ($choixMenu != "A" && $choixMenu != "B" && $choixMenu != "C" && $choixMenu != "D" && $choixMenu != "E" && $choixMenu != "F" && $choixMenu != "G" && $choixMenu != "Q") {
            $choixMenu = strtoupper(readline("choix  invalide : "));
        }
    break;
    }

    if ($choixMenu === "Q") {

        break;

    } elseif ($choixMenu === "A") {

        include "../tableau_multi/A_etat.php";
    }elseif ($choixMenu === "B") {

        include "../tableau_multi/B_nombreReservees.php";
    }elseif ($choixMenu === "C") {

        include "../tableau_multi/C_nombreLibres.php";
    } elseif ($choixMenu === "D") {

        include "../tableau_multi/D_premVide.php";
    } elseif ($choixMenu === "E") {

        include "../tableau_multi/E_dernVide.php";
    } elseif ($choixMenu === "F") {
        include "../tableau_multi/F_reserver.php";
    } elseif ($choixMenu === "G") {
        include "../tableau_multi/G_liberer.php";
    }
}
